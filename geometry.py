import math


# Calculate the distance between two coordinates in a cartesian system.
# pos1 -> {"x": <coordinate_x>, "y": <coordinate_y>}
# pos2 -> {"x": <coordinate_x>, "y": <coordinate_y>}
def get_dist(pos1, pos2):
    return math.sqrt((pos2["x"] - pos1["x"]) * (pos2["x"] - pos1["x"]) +
                     (pos2["y"] - pos1["y"]) * (pos2["y"] - pos1["y"]))


# Calculate the slope of a line between two coordinates in a cartesian system.
# If pos1 == pos2 return infinity
# pos1 -> {"x": <coordinate_x>, "y": <coordinate_y>}
# pos2 -> {"x": <coordinate_x>, "y": <coordinate_y>}
def get_slope(pos1, pos2):
    if pos2["x"] == pos1["x"]:
        return math.inf
    return (pos2["y"] - pos1["y"]) / (pos2["x"] - pos1["x"])


# Calculate the interception with y of a line traced with two points
# pos1 -> {"x": <coordinate_x>, "y": <coordinate_y>}
# pos2 -> {"x": <coordinate_x>, "y": <coordinate_y>}
def get_y_intercept(pos1, pos2):
    return pos1["y"] - get_slope(pos1, pos2) * pos1["x"]


# Calculate the intersection between two lines
# line1 -> {
# "pos1": {"x": <coordinate_x>, "y": <coordinate_y>},
# "pos2": {"x": <coordinate_x>, "y": <coordinate_y>}
# }
# line2 -> {
# "pos1": {"x": <coordinate_x>, "y": <coordinate_y>},
# "pos2": {"x": <coordinate_x>, "y": <coordinate_y>}
# }
# return a pos: {"x": <coordinate_x>, "y": <coordinate_y>}
# If lines are parallel return pos: {"x": math.inf, "y": math.inf}
def get_intersection(line1, line2):
    inter = {}
    slope1 = get_slope(line1["pos1"], line1["pos2"])
    slope2 = get_slope(line2["pos1"], line2["pos2"])
    y_intercept_1 = get_y_intercept(line1["pos1"], line1["pos2"])
    y_intercept_2 = get_y_intercept(line2["pos1"], line2["pos2"])
    if slope1 == slope2:
        inter["x"] = math.inf
        inter["y"] = math.inf
    elif math.isinf(slope1) and not math.isinf(slope2):
        inter["x"] = line1["pos1"]["x"]
        inter["y"] = slope2 * inter["x"] + y_intercept_2
    elif not math.isinf(slope1) and math.isinf(slope2):
        inter["x"] = line2["pos1"]["x"]
        inter["y"] = slope1 * inter["x"] + y_intercept_1
    else:
        inter["x"] = (y_intercept_2 - y_intercept_1) / (slope1 - slope2)
        inter["y"] = slope1 * inter["x"] + y_intercept_1
    return inter


